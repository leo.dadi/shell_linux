#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "redirections.h"


int toExplore(char** paths, char* fun, char** args);
int explore(char* path, char ** args);

int toExplore(char** paths, char* fun, char** args) {

    char *path = getenv("PATH");
    char *tokens[20];
    int numTokens;
    size_t length2 = strlen(fun); 
    splitString(path,tokens,&numTokens, ":");
    int i = 0;

    char *newargv[] = {NULL};
    char    *newenviron[] = { NULL };

    while(tokens[i]){
        // printf("%s \n", tokens[i]);
        size_t length = strlen(tokens[i]);
        char* newString = malloc(length + 2);  // Allocate memory for the new string (+2 for additional '/' and null-terminator)
        snprintf(newString, length + 2 + length2, "%s/%s", tokens[i], fun);
        tokens[i] = newString;  // Assign the new string to tokens[i]
        if(execve(tokens[i], args, newenviron) != -1){
            break;
        }
        i++;
    }
    char cwd[256];  // Assuming the path won't exceed 255 characters
    if (getcwd(cwd, sizeof(cwd)) == NULL) {
        printf("FAILED retrieving current directory");
        return 0;
    }

    strcat(cwd, "/");
    strcat(cwd, fun);
    // printf("cwd %s\n", cwd);
    if(execve(cwd, args, newenviron)==-1){
        printf("Error : no command named %s\n", fun);
        return 0;   
    };
}

int explore(char* fun, char** args){
    char *paths[20];
    toExplore(paths, fun, args);
    return 0;
}

int ok(){
    char fun[] = "ls";
    char *args[] = {NULL, "/bin"};
    explore(fun, args);
}
