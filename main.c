#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "explore.h"
#include "redirections.h"
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#define MAX_TOKENS 15
#define BUFSIZE 200

//functions
char** parse_line(char* buff, char* fun);
void sh_loop();
void printDir();

char path[BUFSIZE];
char cwd[80];

void printDir(){

    char cwd[1024];
    if( !getcwd(cwd, sizeof(cwd))){
        printf("Could not rerieve current directory.\n");
        exit(-1);
    }
    printf("\x1b[32m%s\x1b[0m",cwd);
}

void readLine(char* buff){
    printDir();
    printf(" $ ");

    fgets(buff, 1024, stdin);
    int len = strlen(buff);
    if (len > 0 && buff[len - 1] == '\n'){
        buff[len - 1] = '\0';
    }

}


void sh_loop(){
    char buff[1024];
    while(1){
        
        // // //get all path to explore
        // char* tokens[15];
        // int numTokens=0;
        // toExplore(tokens);
        
        // get input
        char* parsed[2];
        readLine(buff);
        // buff = "/usr/bin/ps -al";
        // printf("\n\nBuffer : %s\n\n", buff);

        int i =redirection(buff, parsed);
        if(i==1){
            // ********* PIPE *********
            int fd[2];
            pid_t childpid1, childpid2;
            // Create the pipe
            if (pipe(fd) == -1) {
                perror("pipe");
                return;
            }
            // Fork the first child process
            childpid1 = fork();
            if (childpid1 == -1) {
                perror("fork");
                return;
            }

            if (childpid1 == 0) {
                // Child process 1: write to the pipe
                close(fd[0]); // Close the unused read end
                dup2(fd[1], STDOUT_FILENO); // Redirect stdout to the write end of the pipe
                close(fd[1]); // Close the write end of the pipe

                char fun[10];
                char** argv = parse_line(parsed[0], fun);
                explore(fun, argv);

                exit(0);
            }

            // Fork the second child process
            childpid2 = fork();
            if (childpid2 == -1) {
                return;
            }

            if (childpid2 == 0) {
                // Child process 2: read from the pipe
                close(fd[1]); // Close the unused write end
                dup2(fd[0], STDIN_FILENO); // Redirect stdin to the read end of the pipe
                close(fd[0]); // Close the read end of the pipe

                char fun[10];
                char** argv = parse_line(parsed[1], fun);
                explore(fun, argv);

                exit(0);
            }

            // Close the pipe ends in the parent process
            close(fd[0]);
            close(fd[1]);

            // Wait for both child processes to complete
            wait(&childpid1);
            wait(&childpid2);

            
        } else if(i==2){
            // fils 1 > fichier
            printf("redirection >\n");
            pid_t child_pid = fork();
            if (child_pid == -1) {
                perror("fork");
                exit(1);
            }

            if (child_pid == 0) {
                // Child process
                char fun[10];
                char** argv = parse_line(parsed[1], fun);

                int fd = open(fun, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
                if (fd == -1) {
                    perror("open");
                    exit(1);
                }

                dup2(fd, STDOUT_FILENO);  // Redirect stdout to the file

                // Execute the command to be run and redirect output to the file
                argv = parse_line(parsed[0], fun);
                
                explore(fun, argv);

                perror("execlp");
                exit(1);
            } else {
                // Parent proces
                wait(&child_pid);
            }
        } else if(i==3){    
            // fils 1 < fils 2
        } else {
            //standard case
            char fun[10];
            char** argv = parse_line(buff, fun);
            // printf("fun %s\n",fun);
            pid_t child_pid = fork();
            if(child_pid== -1){
                printf("Fork failed");
            } else if(child_pid ==0){
                //new proces
                // char* argvs[] =  {NULL};
                explore(fun, argv);
                exit(0);
            } else {
                //parent wait for child to complete
                wait(&child_pid);
            }
        }
    }

    
}

char** parse_line(char* buff, char* fun){
    int len = strlen(buff);
    char arg[100];
    int beginning = 1;
    int current_arg = 0;
    int current_pos = 0;
    char** args = malloc(50*sizeof(char*));
    if(!args){
        printf("Could not allocate space for arguments\n");
        exit(EXIT_FAILURE);
    }
    for(int i=0; i<len; i++){
        if(buff[i]==' ' ){ 
            if(current_pos==0){
                continue;
            }
            arg[current_pos]='\0';      
                args[current_arg]=malloc(sizeof(char)*100);   // if we meet a space : the current argument has ended
                strcpy(args[current_arg],arg);  // add it to the args list at the positio          // increment current arg
                current_pos = 0;        // reset current position
                current_arg++;
        } else {
                arg[current_pos]=buff[i];   // otherwise we add the character to the the argument buffer
                current_pos++;              // and increment the position
        }
    }
    arg[current_pos]='\0';
    args[current_arg]=malloc(sizeof(char)*100);   // if we meet a space : the current argument has ended
    strcpy(args[current_arg],arg);  // add it to the args list at the position
    current_arg++;
    args[current_arg] = NULL;

    // printf(" arg 0 %s\n", args[0]);
    strcpy(fun, args[0]);

    // printf("fun is %s\n", fun);
    // int i = 1;
    // while(args[i]){
    //     argv[i-1]=malloc(sizeof(char)*100);
    //     strcpy(argv[i-1],args[i]);
    //     // printf("%s\n", argv[i-1]);
    //     free(args[i]);
    //     i++;
    // }
    return args;
    
}

void main(){
    printf("Initalizing FFSHELL..\n");

    //try to retrieve PATH
    if(!getenv("PATH")){
        printf("Could not retrieve PATH.\n");
    }
    // printDir();
    //check is buffer can hold path
    if(snprintf(path, BUFSIZE, "%s", getenv("PATH")) >= BUFSIZE){
        fprintf(stderr, "BUFSIZE of %d was too small. Aborting\n", BUFSIZE);
        exit(1);
    }

    char *getcwd(char *buf, size_t size);

    printf("Starting FFSHELL at %s\n" , cwd);

    sh_loop();
}