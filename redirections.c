#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#define MAX_TOKENS 15

void splitString(char* str, char** tokens, int* numTokens, char* delimiter);
int redirection(char* input, char** parsed);


void splitString(char* str, char** tokens, int* numTokens, char* delimiter) {
    *numTokens = 0;
    char* token = strtok(str, delimiter);
    while (token != NULL) {
        tokens[*numTokens] = token;
        (*numTokens)++;
        token = strtok(NULL, delimiter);
    }
}
  
int redirection(char* input, char** parsed){

    char *tokens[MAX_TOKENS];
    int numTokens;
    char delimiter;

    if(strchr(input, '|')){
        splitString(input, parsed, &numTokens, "|");
        return 1;
    } else if(strchr(input, '>')){
        splitString(input, parsed, &numTokens, ">");
        return 2;
    } else if(strchr(input, '<')){
        splitString(input, parsed, &numTokens, "<");
        return 3;
    } else {
        return 0;
    }

}

int man_test(){

    char *path = getenv("PATH");
    char *tokens[20];
    int numTokens;
    splitString(path,tokens,&numTokens, ":");
    int i = 0;
    while(tokens[i]){
        printf("%s ", tokens[i]);
        size_t length = strlen(tokens[i]);
        char* newString = malloc(length + 2);  // Allocate memory for the new string (+2 for additional '/' and null-terminator)
        snprintf(newString, length + 2 , "%s/", tokens[i]);
        tokens[i] = newString;  // Assign the new string to tokens[i]
        printf("new: %s\n", tokens[i]);
        i++;
    }
}

